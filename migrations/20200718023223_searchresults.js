
exports.up = function (knex) {
    return Promise.all([
        knex.schema.createTable('searchResults', function (table) {
            table.increments('id').primary();
            table.string('url');
            table.string('userName')
            table.string('language');
            table.string('fullName');
            table.string('description');
            table.boolean('isActive');
            table
                .timestamp('created_at')
                .notNullable()
                .defaultTo(knex.fn.now());
            table
                .timestamp('updated_at')
                .notNullable()
                .defaultTo(knex.fn.now());
        })
    ]);
};

exports.down = function (knex) {
    return Promise.all([knex.schema.dropTable('searchResults')]);
};

