const test = require('ava');
const request = require('supertest');
const app = require('../app');

test('signIn:invalid body values', async t => {
    t.plan(2);
    const res = await request(app)
        .post('/users/signIn')
        .send({ emailId: "murali@gmail.com", password: "dd" });
    t.is(res.status, 404);
    t.deepEqual(res.body, 'User not Existed');
});
test('signIn:invalid body properties', async t => {
    t.plan(2);
    const res = await request(app)
        .post('/users/signIn')
        .send({ username: "murali@gmail.com", password: "dd" });
    t.is(res.status, 400);
    t.deepEqual(res.body, "bad request");
});

test('signIn: Something wrong Internal', async t => {
    t.plan(1);
    const res = await request(app)
        .post('/users/signIn')
        .send({ emailId: "muraffi@gmail.com", password: "dd" });
    t.is(res.status, 500);

});

test('signIn: Success', async t => {
    t.plan(1);
    const res = await request(app)
        .post('/users/signIn')
        .send({ emailId: "murali@gmail.com", password: "abc" });
    t.is(res.status, 200);


});

test('search by language data: Get All', async t => {

    t.plan(1);
    const res = await request(app)
        .get('/searchresults?language=scala&pageSize=2&pageIndex=0&userName=Murali')

    t.is(res.status, 200);
});

test('show results of username by languge:Get all', async t => {

    t.plan(1);
    const res = await request(app)
        .get('/showresults?userName=Krishna&language=javascript&pageSize=10&pageIndex=0')

    t.is(res.status, 200);
});

test('Search tracking by admin: Get All', async t => {

    t.plan(1);
    const res = await request(app)
        .get('/searchtracking?pageSize=10&pageIndex=0')

    t.is(res.status, 200);
});
