var express = require('express');
var router = express.Router();
const axios = require('axios');
const knex = require('knex');
const dbConfig = require('../knexfile').development;
const fs = require('fs')
var json2xls = require('json2xls');
const moment = require('moment');


/* GET home page. */
router.get('/searchresults', async (req, res, next) => {
  let db;
  try {
    db = knex(dbConfig);
    const response = await axios({
      url: `https://api.github.com/search/repositories?q=language:${req.query.language}&per_page=${req.query.pageSize}&page=${req.query.pageIndex}`,
      method: 'GET',
      headers: { format: 'json', Accept: 'application/json' }
    });

    if (response.data) {
      const resultdata = response.data.items.map(x => ({
        url: `https://api.github.com/search/repositories?q=language:
          ${req.query.language}&per_page=${req.query.pageSize}&page=${req.query.pageIndex}`,
        userName: req.query.userName,
        language: req.query.language,
        fullName: x['full_name'],
        description: x['description'],
        isActive: true
      }))

      await db.transaction(async function (tr) {
        return await db.batchInsert('searchResults', resultdata).transacting(tr);
      });


    }
    db.destroy();
    return res.status(200).json(
      {
        language: response.data.items[0]['language'],
        count: response.data.total_count,
        data: response.data.items.map((x, index) => ({
          id: index + 1 + (req.query.pageIndex * req.query.pageSize),
          name: x['full_name'],
          description: x['description'],
        }))
      }
    )

  } catch (error) {
    db.destroy();
    return res.status(500).json(error)
  }
});

router.get('/showresults', async (req, res, next) => {
  let db;
  try {
    db = knex(dbConfig);
    const pageSize = parseInt(req.query.pageSize);
    const pageIndex = parseInt(req.query.pageIndex);


    const data = await
      db.select('fullName', 'description', 'created_at')
        .from('searchResults')
        .where({ userName: req.query.userName })
        .andWhere({ language: req.query.language })
        .limit(pageSize)
        .offset(pageSize * pageIndex)


    const count = await
      db.select('*')
        .from('searchResults')
        .where({ userName: req.query.userName })
        .andWhere({
          language: req.query.language
        })

    // var xls = json2xls(data);

    // await fs.writeFileSync(`${req.query.language}searched_by${req.query.userName}.xlsx`, xls, 'binary');
    // return res.download(`${req.query.language}searched_by${req.query.userName}.xlsx`);
    db.destroy();
    return res.status(200).json({
      count: count.length,
      data: data.map(x => ({
        fullName: x.fullName,
        description: x.description,
        date: moment(x.created_at).format("YYYY-MM-DD"),
        time: moment(x.created_at).format("hh:mm A")
      }))
    })
  } catch (error) {
    db.destroy();
    return res.status(500).json(error)
  }

});

router.get('/searchtracking', async (req, res, next) => {
  let db;
  try {
    db = knex(dbConfig);
    const pageSize = parseInt(req.query.pageSize);
    const pageIndex = parseInt(req.query.pageIndex);
    const data = await
      db.select('userName', 'language')
        .from('searchResults')

    const result = data.filter((v, i, a) => a.findIndex(t => (JSON.stringify(t) === JSON.stringify(v))) === i)
    db.destroy();
    return res.status(200).json({
      count: result.length,
      data: result.map((x, index) => ({
        id: index + 1,
        userName: x.userName,
        language: x.language,

      })).slice(pageIndex * pageSize, (pageIndex + 1) * pageSize),

    })

  }

  catch (error) {
    db.destroy();
    return res.status(500).json(error)

  }
});

module.exports = router;
